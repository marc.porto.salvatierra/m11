
# PRACTICA LDAPS

 · EDICIÓ FITXERS

> ``` 
> $ vim Dockerfile
> EXPOSE 389 636
> ```

>> · PORT ldap segur -> 636

> ``` 
> $ vim startup.sh
> /usr/sbin/slapd -d3 -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"
> ```

>> · Encem el dimoni amb usuari root en compte d'usuari LDAP.

> ``` 
> $ vim ldap.conf
> TLS_CACERT /etc/ldap/certs/ca.crt
> TLS_KEY /etc/ldap/certs/serverkey.ldap.pem
> ```

> ``` 
> $ vim slapd.conf
> TLSCACertificateFile        /etc/ldap/certs/ca.crt
> TLSCertificateFile          /etc/ldap/certs/servercert.ldap.crt
> TLSCertificateKeyFile       /etc/ldap/certs/serverkey.ldap.pem
> TLSVerifyClient       never
> ```

· Creem certificats CAkey, servekey, peticionCA, server certificat

· Necessitem dues claus privades (CA (Usa per signar certificats) + Servidor Cert(identifica el client/propietari))

1. Creem la nostra propia CA

> - Generem la ca.key -

> - Generem la server.key (skip) 
 	  
> - Generem el certificat propi de la CA + ca.key

>> ```
>> $ openssl req -new -x509 -nodes -sha1 -days 365 -keyout ca.key -out ca.crt
>>
>> Country Name (2 letter code) [AU]:ca
>> State or Province Name (full name) [Some-State]:ca
>> Locality Name (eg, city) []:bcn
>> Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
>> Organizational Unit Name (eg, section) []:inf
>> Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
>> Email Address []:ldap@edt.org
>> ```

2. Verificar

> ```
> $ openssl x509 -noout -text -in ca.crt 
> 		
>   X509v3 Basic Constraints: critical
>    CA:TRUE
> ```

3. Creem el fitxer de configuració d'extensions adicionals. 

> ```
> $ vim ca.conf 
> basicConstraints=CA:FALSE
> extendedKeyUsage=serverAuth
> subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,DNS:ldapserver.exemple.org
>```	

>> - 172.17.0.2 -> Container.

>> - 127.0.0.1 -> Localhost del container.

3. Creem la peticioó de certificat, és un arxiu que emet la CA per poder emetre el certificat.

> ```
> $ openssl req -new -nodes -keyout serverkey.ldap.pem -out serverreq.ldap.csr
>
> Country Name (2 letter code) [AU]:ca
> State or Province Name (full name) [Some-State]:ca
> Locality Name (eg, city) []:bcn
> Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
> Organizational Unit Name (eg, section) []:inf
> Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
> Email Address []:ldap@edt.org
> ``` 

>> - NO PASSPHRASE.

4. CA firma el certificat del servidor.

> ```		
> $ openssl x509 -CA ca.crt -CAkey ca.key -req -in serverreq.ldap.csr -days 365 -extfile ca.conf -CAcreateserial -out servercert.ldap.crt  
> ```		

>> - Tornar a signar el certificat sempre hi quan s'afegeixin altres alternatives name al ca.conf

5. Construim l'imatge del container.

> ```	
> $ docker build -t a190074lr/tls22:ldaps .
> ```	

6. Encendre container.

> ```	
> $ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -p 636:636 -d a190074lr/tls22:ldaps
> ```	

7. Entrar al container.

> ```	
> $ docker exec -it ldap.edt.org /bin/bash
> ```

8. Realitzar comprovacions dins del container.

> - Exemples de connexió client en text plà i en tls/ssl:

>> ```
>> # ldapsearch -x  -H ldap://ldap.edt.org 
>> # ldapsearch -x  -H ldaps://ldap.edt.org 
>> ```

> - Exemples usant startls:

>> ```
>> # ldapsearch -x -Z -H ldap://ldap.edt.org  
>> # ldapsearch -x -ZZ -H ldap://ldap.edt.org 
>> ```

> - Exemple 'erroni', usar starttls sobre una connexió que ja és tls/ssl:

>> ```
>> # ldapsearch -x -ZZ -H ldaps://ldap.edt.org
>> ```

> - Desar e un fitxer el debug:

>> ```
>> # ldapsearch -x -ZZ -H ldap://ldap.edt.org -d1   dn  2> log
>> ```

9. Fer-ho funcionar des de fora del container.

> 9.1 Editar el /etc/hosts

>> ```
>> $ sudo vim /etc/hosts
>>
>> 172.17.0.2	ldap.edt.org ldapserver.exemple.org
>> ```

> 9.2 Editar el /etc/ldap/ldap.conf

>> ```
>> $ sudo vim /etc/ldap/ldap.conf
>>
>> TLS_CACERT	/home/users/inf/hisx2/a190074lr/Documents/M11/UF2. SEGURETAT ACTIVA/LDAPS/ca.crt
>> ```

>>> Ubicació del certificat de la CA, del directori on s'ha planxat el container.


10. Realitzar comprovacions fora del container.

> ```	
> $ ldapsearch -x -LLL -h 172.17.0.2 -s base -b 'dc=edt,dc=org' dn
> ```

> ```	
> $ ldapsearch -x -LLL -Z -h 172.17.0.2 -s base -b 'dc=edt,dc=org' dn
> ```

> ```	
> $ ldapsearch -x -LLL -ZZ-h 172.17.0.2 -s base -b 'dc=edt,dc=org' dn
> ```

> ```	
> $ ldapsearch -x -LLL -H ldaps://172.17.0.2  -s base -b 'dc=edt,dc=org' dn
> ```

> ```	
> $ ldapsearch -x -LLL -H ldaps://172.17.0.2:636  -s base -b 'dc=edt,dc=org' dn
> ```

> ```	
> $ ldapsearch -x -LLL -H ldaps://ldap.edt.org -s base -b 'dc=edt,dc=org' dn
> ```
