#! /bin/bash

echo 1 > /proc/sys/net/ipv4/ip_forward 

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra IP

iptables -A INPUT -s 10.200.245.218 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.218 -j ACCEPT

# fer nat per xarxes docker
iptables -t nat -A POSTROUTING -s 172.18.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE

# Redireccio de ports
# port 5001 port al 2013 de A1
iptables -t nat -A PREROUTING -p tcp --dport 5001 -j DNAT --to 172.18.0.2:2013

# port 5002 porta al port 22 del A1
iptables -t nat -A PREROUTING -p tcp --dport 5002 -j DNAT --to 172.18.0.2:22

# port 6000 va a parar al port 22 del propi router
iptables -t nat -A PREROUTING -p tcp --dport 6000 -j DNAT --to :22
iptables -A FORWARD -i enp1s0 -d 172.18.0.0/16 -j REJECT

# port 80 del nostre pc connecti al gmail
iptables -t nat -A PREROUTING -p tcp --dport 80 -j DNAT --to 217.70.184.56:443
