#! /bin/bash

echo 1 > /proc/sys/net/ipv4/ip_forward 

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra IP

iptables -A INPUT -s 10.200.245.218 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.218 -j ACCEPT

# exemples regles input
iptables -A INPUT -p tcp --dport 13 -j ACCEPT

# tancat el port 2013 per tothom(reject)
iptables -A INPUT -p tcp --dport 2013 -j REJECT

# tancar a tothom el 3013(drop)
iptables -A INPUT -p tcp --dport 3013 -j DROP

# tancar al g25 el port 4013
iptables -A INPUT -p tcp --dport 4013 -s 10.200.245.225 -j DROP
iptables -A INPUT -p tcp --dport 4013 -j ACCEPT

# port tancat a tothom però obert a hisx2, pero tancat a g25 5013
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.225 -j REJECT
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.0/24 -j ACCEPT
iptables -A INPUT -p tcp --dport 5013 -j DROP

# port 6013, obert tothom, tancat aula, obert g25
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.225 -j ACCEPT
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.0/24 -j REJECT
iptables -A INPUT -p tcp --dport 6013 -j ACCEPT

# tancar els ports del 3000 al 8000
#iptables -A INPUT -p tcp --dport 3000:8000 -j REJECT

# barrera final (tancar ports 0:1023)
#iptables -A INPUT -p tcp --dport 1:1024 -j REJECT
