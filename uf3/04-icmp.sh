#! /bin/bash

echo 1 > /proc/sys/net/ipv4/ip_forward 

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra IP
iptables -A INPUT -s 10.200.245.218 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.218 -j ACCEPT

# No permetem fer pings a l'exterior
# iptables -A OUTPUT -p icmp --icmp-type 8 -j DROP

# Nosaltres no permetem fer pings cap al g25
# iptables -A OUTPUT -p icmp --icmp-type 8 -d 10.200.245.225 -j DROP

# La nostra màquina no respon els pings que rep
# iptables -A INPUT -p icmp --icmp-type 0 -j DROP

# La nostra maquina no permet rebre pings
# iptables -A INPUT -p icmp --icmp-type 8 -j DROP

# ----------------------------------------------

# Regles explícites permeto fer ping a la meva màquina
iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT

# Regles explicites per denegar la nostre maquina respondre pings 
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP

