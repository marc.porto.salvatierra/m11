#! /bin/bash

echo 1 > /proc/sys/net/ipv4/ip_forward 

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra IP

iptables -A INPUT -s 10.200.245.218 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.218 -j ACCEPT

# regles output
# podem accedir a qualsevol port 13 de tot el mon
iptables -A OUTPUT -p tcp --dport 13 -j ACCEPT

# No es pot accedir a cap 2013 i 3013 de ningu
iptables -A OUTPUT -p tcp --dport 2013 -j REJECT
iptables -A OUTPUT -p tcp --dport 3013 -j DROP

# Port 4013 de ningu, aula si, g25 no
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.225 -j REJECT
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.0/24 -j ACCEPT 
iptables -A OUTPUT -p tcp --dport 4013 -j REJECT

# port 5013, tothom si, aula no, g25 si
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.225 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.0/24 -j REJECT 
iptables -A OUTPUT -p tcp --dport 5013 -j ACCEPT

# a l'aula nomes es pot accedir al servei ssh (tots els altres serveis xapats)
#iptables -A OUTPUT -p tcp --dport 22 -d 10.200.245.0/24 -j ACCEPT
#iptables -A OUTPUT -d 10.200.245.0/24 -j REJECT
