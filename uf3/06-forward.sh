#! /bin/bash

#echo 1 > /proc/sys/net/ipv4/ip_forward 

# regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra IP
iptables -A INPUT -s 10.200.245.218 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.218 -j ACCEPT

# fer nat per les xarxes docker netA i netB
iptables -t nat -A POSTROUTING -s 172.19.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE

# regles forward 
# xarxa A no pot accedir a xarxa B
#iptables -A FORWARD -s 172.19.0.0/16 -d 172.22.0.0/16 -j REJECT # no permet pasar paquets a xarxa b

# xarxa A no pota accedir a B1
#iptables -A FORWARD -s 172.19.0.0/16 -d 172.22.0.2/16 -j REJECT

# xarxa A no pot accedir a cap port 13 (de enlloc)
#iptables -A FORWARD -p tcp -s 172.19.0.0/16 --dport 13 -j REJECT

# xarxa A no pot accedir al port 2013 de la xarxa B
# iptables -A FORWARD -p tcp -s 172.18.0.0/16 -d 172.22.0.0/16 --dport 2013 -j REJECT

# la xarxa A pot navegar per internet(80) pero res mes a l'exterior, sortida ethernet publica
#iptables -A FORWARD -s 172.18.0.0/16 -o enp1s0 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -d 172.18.0.0/16 -i enp1s0 -p tcp --sport 80 -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A FORWARD -s 172.18.0.0/16 -o enp1s0 -j REJECT
#iptables -A FORWARD -d 172.18.0.0/16 -i enp1s0 -j REJECT

# xarxa A pot accedir al port 2013 de totes les xarxes d'internet (exterior) excepte l'aula 2hisx
iptables -A FORWARD -s 172.18.0.0/16 -o enp1s0 -p tcp --dport 2013 -d 10.200.245.0/24 -j REJECT
iptables -A FORWARD -s 10.200.245.0/24 -p tcp --sport 2013 -i enp1s0 -d 172.18.0.0/16 -j REJECT
iptables -A FORWARD -s 172.18.0.0/16 -o enp1s0 -p tcp --dport 2013 -j ACCEPT
iptables -A FORWARD -d 172.18.0.0/16 -i enp1s0 -p tcp --sport 2013 -m state --state RELATED,ESTABLISHED -j ACCEPT

